/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ws;

import java.math.BigInteger;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.xml.ws.Service;
import javax.xml.ws.WebServiceRef;
import manufacture.mibookair.MiBookAir;
import manufacture.mipad.ManufactureWS;
import manufacture.mipad.Order;
import model.OnTransit;
import model.Warehouse;

/**
 *
 * @author snw
 */
@WebService(serviceName = "WarehouseWS")
@Stateless()
public class WarehouseWS {
	@WebServiceRef(wsdlLocation = "WEB-INF/wsdl/54.186.176.247_8080/ManufactureWS/ManufactureWS_MiBookAir.wsdl")
	private manufacture.mibookair.ManufactureWS service_1;
	@WebServiceRef(wsdlLocation = "WEB-INF/wsdl/54.186.176.247_8080/ManufactureWS/ManufactureWS_MiPad.wsdl")
	private ManufactureWS service;
	private Warehouse wh = Warehouse.getInstance();

	@PersistenceContext(unitName = "soen487-warehouse-pu")
	private EntityManager em;

	public void persist(Object object) {
		em.persist(object);
	}

	/**
	 * Web service operation
	 */
	@WebMethod(operationName = "shipGood")
	public Integer shipGood(@WebParam(name = "type") final String type
					, @WebParam(name = "amount") final Integer amount) {
		// Get quantity of the item first
		String typeName = type.toLowerCase();
		int quantity = wh.getInventory().get(typeName);
		if (quantity >= amount)
		{
			System.out.println("Inventory is " + wh.getInventory());
			wh.getInventory().put(typeName, quantity - amount);

			return amount;
		}
		else
		{
			Integer amountGoingToReceive = 0;
			if (typeName.equals("miphone"))
			{
				

			}
			else if (typeName.equals("mipad"))
			{
				amountGoingToReceive = (orderMiPad(wh.getName()
								, Warehouse.DEFAULT_SHIP_QUANTITY)).getQuantity();
			}
			else if (typeName.equals("mipod"))
			{

			}
			else if (typeName.equals("mibook"))
			{

			}
			else if (typeName.equals("mibookpro"))
			{

			}
			else if (typeName.equals("mibookair"))
			{
				amountGoingToReceive = (orderMiBookAir(wh.getName()
								, Warehouse.DEFAULT_SHIP_QUANTITY)).getQuantity();
			}

			return amountGoingToReceive;
		}
	}

	private Order orderMiPad(java.lang.String wareHouseName, java.lang.Integer quantity) {
		// Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
		// If the calling of port operations may lead to race condition some synchronization is required.
		manufacture.mipad.ManufactureWSMiPad port = service.getManufactureWSMiPadPort();
		return port.shipToWarehouse(wareHouseName, quantity);
	}

	private manufacture.mibookair.Order orderMiBookAir(java.lang.String wareHouseName, java.lang.Integer quantity) {
		// Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
		// If the calling of port operations may lead to race condition some synchronization is required.
		manufacture.mibookair.ManufactureWSMiBookAir port = service_1.getManufactureWSMiBookAirPort();
		return port.shipToWarehouse(wareHouseName, quantity);
	}

	/**
	 * Web service operation
	 */
	@WebMethod(operationName = "inquiry")
	public String inquiry() {
		return inquiryMiBookAir().toString();
	}

	private MiBookAir inquiryMiBookAir() {
		// Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
		// If the calling of port operations may lead to race condition some synchronization is required.
		manufacture.mibookair.ManufactureWSMiBookAir port = service_1.getManufactureWSMiBookAirPort();
		return port.inquiry();
	}


}
