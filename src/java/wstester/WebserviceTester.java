/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package wstester;

import manufacture.mibookair.MiBookAir;
import manufacture.mibookair.Order;
import model.Warehouse;

/**
 *
 * @author snw
 */
public class WebserviceTester {
	public static void main(String[] args)
	{
		Warehouse war = Warehouse.getInstance();
		System.out.println(war.getName());
		Order order = WebserviceTester.shipToWarehouse(war.getName(), 34);
		System.out.println(order.getCustomerRef());
	}

	private static MiBookAir inquiry() {
		manufacture.mibookair.ManufactureWS service = new manufacture.mibookair.ManufactureWS();
		manufacture.mibookair.ManufactureWSMiBookAir port = service.getManufactureWSMiBookAirPort();
		return port.inquiry();
	}

	private static Order shipToWarehouse(java.lang.String wareHouseName, java.lang.Integer quantity) {
		manufacture.mibookair.ManufactureWS service = new manufacture.mibookair.ManufactureWS();
		manufacture.mibookair.ManufactureWSMiBookAir port = service.getManufactureWSMiBookAirPort();
		return port.shipToWarehouse(wareHouseName, quantity);
	}
}
